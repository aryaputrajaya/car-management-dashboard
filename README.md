## Name
Car management dasboard

## Description
sebuah HTTP Server yang dapat digunakan untuk melakukan manajemen data mobil~

 [ERD](blob:https://web.whatsapp.com/276039b8-6748-4a85-b27a-696d1e00d316)


End Point : 

Client

1. Halaman Index = http://localhost:8020/
2. Halaman create = http://localhost:8020/cars/create?
3. Halaman edit = http://localhost:8020/edit-car/8


API 

1. Get All Car (GET) = http://localhost:8020/cars
2. Create CAR (POST) = http://localhost:8020/cars/create 
3. Get car edit (GET) = http://localhost:8020/cars/:id
4. Post Update (POST) = http://localhost:8020/edit-car/:id
5. Hapus (DELETE) = http://localhost:8020/cars/:id/delete






